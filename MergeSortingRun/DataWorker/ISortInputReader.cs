﻿using System;

namespace MergeSortingRun.DataWorker
{
    public interface ISortInputReader:IDisposable
    {
        bool AtEnd { get; }
        string GetNext();
    }
}
