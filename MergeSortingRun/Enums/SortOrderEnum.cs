﻿namespace MergeSortingRun.Enums
{
    public enum SortOrderEnum
    {
        Asc,
        Desc
    }
}
