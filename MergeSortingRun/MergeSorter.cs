﻿using MergeSortingRun.DataWorker;
using MergeSortingRun.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MergeSortingRun
{
    enum ReadTarget
    {
        None,
        First,
        Second,
        Both
    }
    public class MergeSorter
    {

        public void Sort(SortOrderEnum sortOrder, SortDataType sortDataType, Stream resultStream, List<Stream> inputStreams)
        {
            if (resultStream == null)
                throw new ArgumentNullException(nameof(resultStream));

            if (inputStreams == null)
                throw new ArgumentNullException(nameof(inputStreams));

            if (inputStreams.Count == 0)
                throw new ArgumentException(nameof(inputStreams), "Необходимо указать хотя бы один файл с исходными данными");

            var inpCount = inputStreams.Count;
            if (inpCount == 1)
            {
                inputStreams[0].Seek(0, SeekOrigin.Begin);
                inputStreams[0].CopyTo(resultStream);
                return;
            }

            if (inpCount == 2)
            {
                // sorting
                var streamReader1 = new StreamReader(inputStreams[0]);
                var streamReader2 = new StreamReader(inputStreams[1]);
                var streamWriter = new StreamWriter(resultStream);

                string current1="";
                string current2="";
                var readTarget = ReadTarget.Both;
                while (streamReader1.EndOfStream==false && streamReader2.EndOfStream == false)
                {
                    // because this is not array,we cant get alwayse same index
                    switch (readTarget)
                    {
                        case ReadTarget.First:
                             current1 = streamReader1.ReadLine();                             
                            break;
                        case ReadTarget.Second:
                            current2 = streamReader2.ReadLine();
                            break;
                        case ReadTarget.Both:
                            current1 = streamReader1.ReadLine();
                            current2 = streamReader2.ReadLine();
                            break;                                              
                    }

                    var compRes = string.Compare(current1, current2);
                    if (compRes < 0)
                    {
                        streamWriter.WriteLine(current1);
                        readTarget = ReadTarget.First;
                        
                    }
                    if (compRes == 0)
                    {
                        streamWriter.WriteLine(current1);
                        streamWriter.WriteLine(current2);
                        readTarget = ReadTarget.Both;
                    }
                    if (compRes > 0)
                    {
                        streamWriter.WriteLine(current2);
                        readTarget = ReadTarget.Second;
                    }
                }

                // we have rest of one stream
                while(streamReader1.EndOfStream == false)
                {
                    current1 = streamReader1.ReadLine();
                    streamWriter.WriteLine(current1);
                }
                while (streamReader2.EndOfStream == false)
                {
                    current2 = streamReader2.ReadLine();
                    streamWriter.WriteLine(current2);
                }


                streamWriter.Flush();
                return;
            }

            //recurcive
            // delimer to half
            var newCount = inpCount / 2;
            var stream1 = new MemoryStream(); // TODO Change to file
            var stream2 = new MemoryStream();
            Sort(sortOrder, sortDataType, stream1, inputStreams.GetRange(0, newCount));
            Sort(sortOrder, sortDataType, stream2, inputStreams.GetRange(newCount + 1, inpCount - newCount));

            Sort(sortOrder, sortDataType, resultStream, new List<Stream> { stream1, stream2 });




        }

        public void Sort(SortOrderEnum sortOrder, SortDataType sortDataType, ISortResultWriter resultWriter, List<ISortInputReader> sortInputReaders)
        {
            if (resultWriter == null)
                throw new ArgumentNullException(nameof(resultWriter));

            if (sortInputReaders == null)
                throw new ArgumentNullException(nameof(sortInputReaders));

            if (sortInputReaders.Count == 0)
                throw new ArgumentException(nameof(sortInputReaders), "Необходимо указать хотя бы один файл с исходными данными");

            try
            {
                // TODO пока проще раделить по типам, придумать как обойсти без двух методов
                if (sortDataType == SortDataType.sort_int)
                    sortInt(sortOrder, resultWriter, sortInputReaders);
                if (sortDataType == SortDataType.sort_string)
                    sortString(sortOrder, resultWriter, sortInputReaders);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                foreach (var oneReader in sortInputReaders)
                {
                    oneReader.Dispose();
                }
                resultWriter.Dispose();
            }
        }

        private void sortString(SortOrderEnum sortOrder, ISortResultWriter resultWriter, List<ISortInputReader> sortInputReaders)
        {

        }

        private void sortInt(SortOrderEnum sortOrder, ISortResultWriter resultWriter, List<ISortInputReader> sortInputReaders)
        {
            var firstRow = new int?[sortInputReaders.Count];
            // вычислим минимальное в первой строке
            var setFirst = false;
            var firstVal = 0;
            for (var nppReader = 0; nppReader < sortInputReaders.Count; nppReader++)
            {
                var oneReader = sortInputReaders[nppReader];
                if (oneReader.AtEnd) continue;

                var oneValStr = oneReader.GetNext();
                if (int.TryParse(oneValStr, out int oneVal))
                {
                    firstRow[nppReader] = oneVal;
                    if (setFirst == false) { firstVal = oneVal; setFirst = true; }
                    else if (oneVal < firstVal) firstVal = oneVal;
                }
            }


        }
    }
}
