﻿using System;
using MergeSortingRun;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MergeSortingRun.Enums;
using MergeSortingRun.DataWorker;
using UnitTests.Mocks;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class MergeSorterInputTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SortBadInput1()
        {
            var sorter = new MergeSorter();
            var lsReaders = new List<ISortInputReader>()
            {
                new InputReaderByArray()
            };
            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, null, lsReaders);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SortBadInput2()
        {
            var sorter = new MergeSorter();
            var resWriter = new ResultWriterToArray();
            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resWriter, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SortBadInput3()
        {
            var sorter = new MergeSorter();
            var lsReaders = new List<ISortInputReader>();
            var resWriter = new ResultWriterToArray();
            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resWriter, lsReaders);
        }

    }
}
