﻿using System;
using MergeSortingRun;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MergeSortingRun.Enums;
using MergeSortingRun.DataWorker;
using UnitTests.Mocks;
using System.Collections.Generic;
using System.IO;

namespace UnitTests
{
    [TestClass]
    public class MergeSorterTest
    {
 
        [TestMethod]       
        public void SortCalledDispose()
        {
             var sorter = new MergeSorter();
             var resWriter= new ResultWriterToArray();
            var inp1 = new InputReaderByArray();
            inp1.Vals = new List<string>()
            {
                "1","4","7"
            };
            var inp2 = new InputReaderByArray();
            inp2.Vals = new List<string>()
            {
                "1","2","6","8"
            };

            var lsReaders = new List<ISortInputReader>()
            {
                inp1,inp2
            };

            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resWriter, lsReaders);

            Assert.IsTrue(inp1.IsCalledDispose);
            Assert.IsTrue(inp2.IsCalledDispose);
            Assert.IsTrue(resWriter.IsCalledDispose);
        }


        
        public void SortAscInt2ArrayGood()
        {
            var sorter = new MergeSorter();
            var resWriter = new ResultWriterToArray();
            var inp1 = new InputReaderByArray();
            inp1.Vals = new List<string>()
            {
                "1","4","7"
            };
            var inp2 = new InputReaderByArray();
            inp2.Vals = new List<string>()
            {
                "1","2","6","8"
            };

            var lsReaders = new List<ISortInputReader>()
            {
                inp1,inp2
            };

            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resWriter, lsReaders);
                        
            Assert.AreEqual("1", resWriter.ResultArray[0]);
            Assert.AreEqual("1", resWriter.ResultArray[1]);
            Assert.AreEqual("2", resWriter.ResultArray[2]);
            Assert.AreEqual("4", resWriter.ResultArray[3]);
            Assert.AreEqual("6", resWriter.ResultArray[4]);
            Assert.AreEqual("6", resWriter.ResultArray[5]);
            Assert.AreEqual("8", resWriter.ResultArray[6]);         
        }

        [TestMethod]
        public void SortStreamAscInt1ArrayGood()
        {
            var inpStream=new MemoryStream();
            var streamWriter = new StreamWriter(inpStream);
            streamWriter.WriteLine("1");
            streamWriter.WriteLine("4");
            streamWriter.WriteLine("7");
            streamWriter.Flush();
            inpStream.Seek(0, SeekOrigin.Begin);
            var resStream= new MemoryStream();

            var sorter = new MergeSorter();
            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resStream, new List<Stream> { inpStream });

            resStream.Seek(0, SeekOrigin.Begin);
            var streamReader = new StreamReader(resStream);
            Assert.AreEqual("1", streamReader.ReadLine());
            Assert.AreEqual("4", streamReader.ReadLine());
            Assert.AreEqual("7", streamReader.ReadLine());
        }

        [TestMethod]
        public void SortStreamAscInt2ArrayGood()
        {
            var inpStream1 = new MemoryStream();
            var streamWriter = new StreamWriter(inpStream1);
            streamWriter.WriteLine("1");
            streamWriter.WriteLine("4");
            streamWriter.WriteLine("7");
            streamWriter.Flush();
            inpStream1.Seek(0, SeekOrigin.Begin);

            var inpStream2 = new MemoryStream();
            streamWriter = new StreamWriter(inpStream2);
            streamWriter.WriteLine("1");
            streamWriter.WriteLine("2");
            streamWriter.WriteLine("6");
            streamWriter.WriteLine("8");
            streamWriter.Flush();
            inpStream2.Seek(0, SeekOrigin.Begin);


            var resStream = new MemoryStream();

            var sorter = new MergeSorter();
            sorter.Sort(SortOrderEnum.Asc, SortDataType.sort_int, resStream, new List<Stream> { inpStream1,inpStream2 });

            resStream.Seek(0, SeekOrigin.Begin);
            var streamReader = new StreamReader(resStream);
            Assert.AreEqual("1", streamReader.ReadLine());
            Assert.AreEqual("1", streamReader.ReadLine());
            Assert.AreEqual("2", streamReader.ReadLine());
            Assert.AreEqual("4", streamReader.ReadLine());
            Assert.AreEqual("6", streamReader.ReadLine());
            Assert.AreEqual("7", streamReader.ReadLine());
            Assert.AreEqual("8", streamReader.ReadLine());
        }

    }

}
