﻿using System.Collections.Generic;
using MergeSortingRun.DataWorker;

namespace UnitTests.Mocks
{
    internal class InputReaderByArray : ISortInputReader
    {
        public bool IsCalledDispose = false;

        public List<string> Vals { get; set; }
        public int currentIndex { get; set; }
               
        public InputReaderByArray()
        {
            Vals = new List<string>();
            currentIndex = 0;
        }

        public bool AtEnd { get => currentIndex == Vals.Count; }

        public void Dispose()
        {
            IsCalledDispose = true;
        }

        public string GetNext()
        {
            var ret = Vals[currentIndex];
            currentIndex++;
            return ret;
        }
    }
}
