﻿using MergeSortingRun.DataWorker;
using System.Collections.Generic;

namespace UnitTests.Mocks
{
    public class ResultWriterToArray : ISortResultWriter
    {
        public List<string> ResultArray { get; set; }

        public bool IsCalledDispose = false;

        public ResultWriterToArray()
        {
            ResultArray = new List<string>();
        }


        public void Dispose()
        {
            IsCalledDispose = true;
        }
    }
}
